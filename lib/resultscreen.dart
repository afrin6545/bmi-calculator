import 'package:bmi_calculator/bmicalculatescreen.dart';
import 'package:bmi_calculator/bmimodel.dart';
import 'package:flutter/material.dart';

class ResultScreen extends StatelessWidget {
  final BMIModel bmiModell;

  ResultScreen({Key? key, required this.bmiModell}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image(
                  height: 250,
                  width: 250,
                  image: bmiModell.isnormal
                      ? AssetImage('assets/images/happy.jpeg')
                      : AssetImage('assets/images/sad.jpeg'),
                ),
              ),
              Text(
                "Your BMI is "+ (bmiModell.bmi.round()).toString(),
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Colors.red),
              ),

              SizedBox(height: 10),
              Text(
                (bmiModell.comments),
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: Colors.grey),
              ),
              SizedBox(height: 20),
              Container(
                margin: const EdgeInsets.only(left: 22.0, right: 22.0),
                height: 48,
                width: double.infinity,
                color: Colors.pink,
                child: TextButton(onPressed: (){

                  Navigator.pop(
                    context,
                    MaterialPageRoute(builder: (context) => BMICalculateScreen() ),
                  );



                },
                  child: Text("Calculate again",style: TextStyle(color: Colors.white,fontSize: 20),),

                ),
              ),
    


            ],
          ),
        ),
      ),
    );
  }
}
