import 'package:bmi_calculator/bmimodel.dart';
import 'package:bmi_calculator/resultscreen.dart';
import 'package:flutter/material.dart';

class BMICalculateScreen extends StatefulWidget {
  const BMICalculateScreen({Key? key}) : super(key: key);

  @override
  bmiCalculateScreenState createState() => bmiCalculateScreenState();
}

class bmiCalculateScreenState extends State<BMICalculateScreen> {
  double _heightOfUser = 100.00;
  double _weightOfUser = 50.00;
  double bmi = 0.0;
  late BMIModel bmiModel ;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Container(
                  child: Image(
                    height: 250,
                    width: 250,
                    image: AssetImage('assets/images/heart.jpeg'),
                  ),
                ),
              ),
              Text(
                "BMI Calculator",
                style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    color: Colors.red),
              ),
              Text(
                "We care about your health",
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "Height(cm)",
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w500,
                    color: Colors.grey),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: Slider(
                  min: 50,
                  max: 250,
                  onChanged: (height) {
                    setState(() {
                      _heightOfUser = height;
                    });
                  },
                  value: _heightOfUser,
                  divisions: 100,
                  activeColor: Colors.pink,
                  label: "$_heightOfUser",
                ),
              ),
              Text(
                "$_heightOfUser cm",
                style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w500,
                    color: Colors.red),
              ),
              SizedBox(
                height: 40,
              ),
              Text(
                "Weight(kg)",
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w500,
                    color: Colors.grey),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: Slider(
                  min: 30,
                  max: 200,
                  onChanged: (weight) {
                    setState(() {
                      _weightOfUser = weight;
                    });
                  },
                  value: _weightOfUser,
                  divisions: 100,
                  activeColor: Colors.pink,
                  label: "$_heightOfUser",
                ),
              ),
              Text(
                "$_weightOfUser kg",
                style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w500,
                    color: Colors.red),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                margin: const EdgeInsets.only(left: 22.0, right: 22.0),
                height: 48,
                width: double.infinity,
                color: Colors.pink,
                child: TextButton(onPressed: (){
                  bmi = _weightOfUser/((_heightOfUser/100)*(_heightOfUser/100));

                  if(bmi<18.5)
                    {double bmiII=18.5;
                    double weight = (bmiII*(_heightOfUser/100)*(_heightOfUser/100))-_weightOfUser;
                      bmiModel = BMIModel(bmi, false,"You are underweighted"+"You have to gain weight minimum " +weight.round().toString()+ "kg to be fit.");
                      
                    }
                  else if(bmi>=18.5 && bmi<=25)
                  {
                    bmiModel = BMIModel(bmi, true,"Congrats ! You are fit.");
                  }
                  else if(bmi>25 && bmi<=30)
                  {
                    double bmiII = 25.0;
                    double weight = _weightOfUser -
                        (bmiII * (_heightOfUser / 100) * (_heightOfUser / 100));
                    bmiModel = BMIModel(bmi, false,"You are overweighted."+"You have to loose weight minimum " +weight.round().toString()+
                        "kg to be fit.");
                   

                  }
                  else
                  {
                    double bmiII = 25.0;
                    double weight = _weightOfUser -
                        (bmiII * (_heightOfUser / 100) * (_heightOfUser / 100));
                    bmiModel = BMIModel(bmi, false,"You are obsesd."+"You have to loose weight minimum " +weight.round().toString()+
                        "kg to be fit.");
                    
                  }










                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) =>  ResultScreen(bmiModell: bmiModel)),
                  );


                },
                  child: Text("Calculate BMI",style: TextStyle(color: Colors.white,fontSize: 20),),

                ),
              )

            ],
          ),
        ),
      ),
    );
  }
}
